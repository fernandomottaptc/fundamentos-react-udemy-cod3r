import React, { useState } from "react";
import "./Mega.css";

export default (props) => {
    function gerarNumeroNaoContido(min, max, array) {
        const aleatorio = parseInt(Math.random() * (max - min)) + min;
        return array.includes(aleatorio) ? gerarNumeroNaoContido(min, max, array) : aleatorio;
    }

    function gerarNumeros(qtde) {
        const numeros = Array(qtde)
            .fill(0)
            .reduce((nums) => {
                const novoNumero = gerarNumeroNaoContido(1, 61, nums);
                //console.log([...nums, novoNumero])
                return [...nums, novoNumero];
            }, [])
            .sort((n1, n2) => n1 - n2);
        return numeros;
    }
    const [qtdade, setQtdade] = useState(props.qtde || 6);
    const numerosIniciais = Array(qtdade).fill(0);
    const [numeros, setNumeros] = useState(numerosIniciais);

    return (
        <div className="Mega">
            <h2>Números Sorteados</h2>
            <h3>{numeros.join("  ")}</h3>
            <div>
                <label>Qtde de Números: </label>
                <input
                    min="6"
                    max="15"
                    type="number"
                    value={qtdade}
                    onChange={(e) => setQtdade(parseInt(e.target.value))}
                ></input>
            </div>
            <button onClick={(_) => setNumeros(gerarNumeros(qtdade))}>Gerar Números</button>
        </div>
    );
};
