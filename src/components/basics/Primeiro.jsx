import React from "react";

export default function Primeiro() {
  const msg = "Seja bem vindo(a)";
  return (
    <div>
      <h2>Primeiro Componente 2</h2>
      <p>{msg}</p>
    </div>
  );
}
