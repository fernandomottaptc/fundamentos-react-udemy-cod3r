import React, { cloneElement } from "react";

export default (props) => {
    return (
        <React.Fragment>
            {/* {cloneElement(props.children, {...props})} */}
            {props.children.map((el, i) => {
                return cloneElement(el, { ...props, key: i });
            })}
        </React.Fragment>
    );
};
