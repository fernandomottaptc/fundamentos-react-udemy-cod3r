import React from "react";


export default (props) => {
    return (
        <div>
            <button onClick={props.inc}>+</button>
            <button onClick={props.dec}>-</button>
            {/* Ou passar uma função arrow no click e voltar inc para uma função normal */}
        </div>
    );
};
