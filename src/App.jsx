import React from "react";
import "./App.css";
import Primeiro from "./components/basics/Primeiro";
import ComParametro from "./components/basics/ComParametro";
import Fragmento from "./components/basics/Fragmento";
import Aleatorio from "./components/basics/Aleatorio";
import Card from "./components/layout/Card";
import Familia from "./components/basics/Familia";
import FamiliaMembro from "./components/basics/FamiliaMembro";
import ListaAlunos from "./components/repeticao/ListaAluno";
import TabelaProdutos from "./components/repeticao/TabelaProdutos";
import ParOuImpar from "./components/condicional/ParOuImpar";
import UsuarioInfo from "./components/condicional/UsuarioInfo";
import DiretaPai from "./components/comunicacao/DiretaPai";
import IndiretaPai from "./components/comunicacao/IndiretaPai";
import Input from "./components/formulario/Input";
import Contador from "./components/contador/Contador";
import MegaSena from "./components/mega/Mega";

export default (props) => (
    <div className="App">
        <h1>Fundamentos React</h1>
        <div className="Cards">
            <Card titulo="Mega Sena" color="#F2AE72">
                <MegaSena qtde={7}/>
            </Card>
            <Card titulo="Contador" color="">
                <Contador numeroInicial={10} />
            </Card>
            <Card titulo="Componente Controlado" color="#69D2E7">
                <Input />
            </Card>
            <Card titulo="Comunicação Indireta" color="#1FDA9A">
                <IndiretaPai />
            </Card>
            <Card titulo="Comunicação Direta">
                <DiretaPai />
            </Card>
            <Card titulo="Renderização Condicional">
                <UsuarioInfo usuario={{ nome: "Fernando" }} />
            </Card>
            <Card titulo="Desafio Repetição">
                <ParOuImpar numero="20" />
            </Card>
            <Card titulo="Desafio Repetição" color="#F2AE72">
                <TabelaProdutos />
            </Card>
            <Card titulo="Repetição">
                <ListaAlunos />
            </Card>
            <Card titulo="Componente com Filhos">
                <Familia sobrenome="Ferreira">
                    <FamiliaMembro nome="Fulano" />
                    <FamiliaMembro nome="Ciclano" />
                    <FamiliaMembro nome="Fernando" />
                </Familia>
            </Card>
            <Card titulo="Desafio Aleatório" color="#69D2E7">
                <Aleatorio min={10} max={60} />
            </Card>
            <Card titulo="Fragmento" color="#1FDA9A">
                <Fragmento />
            </Card>
            <Card titulo="Com Parâmetro" color="#F2AE72">
                <ComParametro titulo="Situação do Aluno" aluno="Fernando" nota={9.4} />
            </Card>
            <Card titulo="Primeiro">
                <Primeiro />
            </Card>
        </div>
    </div>
);
