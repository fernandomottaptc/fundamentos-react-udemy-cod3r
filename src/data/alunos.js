export default [
    {id: 1, nome: 'Fernando', nota: 9.3},
    {id: 2, nome: 'Ana', nota: 4.3},
    {id: 3, nome: 'Juliana', nota: 5.5},
    {id: 4, nome: 'Gabriel', nota: 3.0},
    {id: 5, nome: 'Claudio', nota: 8.0},
    {id: 6, nome: 'Janaina', nota: 6.0},
    {id: 7, nome: 'Franciely', nota: 10},
    {id: 8, nome: 'Diego', nota: 7.7},
    {id: 9, nome: 'Fulano', nota: 9.0},
]