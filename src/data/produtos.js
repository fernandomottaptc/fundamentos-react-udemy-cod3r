const produtos = [
    {id: 1, nome: "Caneta", preco: 10.00},
    {id: 2, nome: "Lapis", preco: 3.20},
    {id: 3, nome: "Caderno", preco: 15.30},
    {id: 4, nome: "Notebook", preco: 9855.22},
    {id: 5, nome: "Monitor", preco: 700.29},
    {id: 6, nome: "Cadeira", preco: 352.11},
    {id: 7, nome: "Teclado", preco: 80.00},
    {id: 8, nome: "Mouse", preco: 60.00},
]

export default produtos